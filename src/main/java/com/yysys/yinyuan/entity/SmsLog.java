package com.yysys.yinyuan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:18
 */
@Data
@TableName("t_smslog")
@NoArgsConstructor
public class SmsLog {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer type;
    private String info;
    private String phone;
    private Date ctime;

    public SmsLog(Integer type, String info, String phone, Date ctime) {
        this.type = type;
        this.info = info;
        this.phone = phone;
        this.ctime = ctime;
    }
}
