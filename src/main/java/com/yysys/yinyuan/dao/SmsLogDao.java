package com.yysys.yinyuan.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yysys.yinyuan.entity.SmsLog;
import org.springframework.stereotype.Component;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:20
 */
@Component
public interface SmsLogDao extends BaseMapper<SmsLog> {
}
