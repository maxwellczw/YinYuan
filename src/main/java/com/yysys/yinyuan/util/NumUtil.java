package com.yysys.yinyuan.util;

import java.util.Random;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:13
 */
public class NumUtil {
    /**
     * 随机生成指定位数的数字
     * */
    public static int createNum(int len){
        Random random=new Random();
        //4 1000-9999 0-9000+1000
        return (int)(Math.pow(10,len-1))+
                random.nextInt((int)(Math.pow(10,len)-Math.pow(10,len-1)));
    }
}
