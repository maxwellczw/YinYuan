package com.yysys.yinyuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YinyuanApplication {

    public static void main(String[] args) {
        SpringApplication.run(YinyuanApplication.class, args);
    }

}
