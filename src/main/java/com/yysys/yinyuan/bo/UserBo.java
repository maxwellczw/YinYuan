package com.yysys.yinyuan.bo;

import lombok.Data;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-05 09:29
 */
@Data
public class UserBo {
    private String phone;
    private String pass;
}