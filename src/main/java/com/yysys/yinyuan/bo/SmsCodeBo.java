package com.yysys.yinyuan.bo;

import lombok.Data;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:22
 */
@Data
public class SmsCodeBo {
    private String phone;
    private Integer code;
}
