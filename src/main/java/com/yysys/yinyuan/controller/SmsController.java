package com.yysys.yinyuan.controller;

import com.yysys.yinyuan.bo.SmsCodeBo;
import com.yysys.yinyuan.service.intf.SmsService;
import com.yysys.yinyuan.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: IdlerApi
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2021-07-02 16:16
 */
@RestController
@RequestMapping("/api/sms/")
@CrossOrigin
public class SmsController {

    @Autowired
    private SmsService smsService;
    //发送短信验证码
    @PostMapping("sendcode.do")
    public R sendCode(String phone){
        return smsService.sendCode(phone);
    }
    //校验短信验证码
    @PostMapping("checkcode.do")
    public R checkCode(SmsCodeBo bo){
        return smsService.checkCode(bo);
    }
    //查询短信发送记录
    @GetMapping("all.do")
    public R all(){
        return smsService.all();
    }
}
