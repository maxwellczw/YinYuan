package com.yysys.yinyuan.third;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:36
 */
public class JedisUtil {
    public static final String HOST="47.100.113.61";
    public static final Integer PORT=6379;
    public static final String PASS="zzjava";
    private static Jedis client;
    static {
        JedisPool pool=new JedisPool(HOST,PORT);
        client=pool.getResource();
        client.auth(PASS);
    }
    //新增
    public static void addStr(String key,String val,long seconds){
        client.setex(key,seconds,val);
    }
    //查询
    public static String getStr(String key){
        return client.get(key);
    }
    //校验
    public static boolean checkKey(String key){
        return client.exists(key);
    }
    //返回剩余有效期
    public static long ttl(String key){
        return client.ttl(key);
    }
    //删除
    public static boolean delKey(String key){
        return client.del(key)>0;
    }
}
