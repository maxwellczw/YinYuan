package com.yysys.yinyuan.third;

import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.dysmsapi20170525.Client;
/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:02
 */
public class AliSmsUtil {
    //平台的密钥 不要公开到公众平台
    private static final String accessKeyId="LTAI5t8QA19CRR2V15oHyHW8";//
    private static final String accessKeySecret="OqUKmRFUnAYm0frzUc43uKnk5BNoaP";//
    /**
     * 发送短信验证码
     * @param phone 手机号
     * @param code 短信验证码*/
    public static boolean sendCode(String phone,int code){
        //1.创建配置对象
        Config config = new Config()
                .setAccessKeyId(accessKeyId)
                .setAccessKeySecret(accessKeySecret);
        //2.访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        Client client;
        try {
            //3.实例化客服端对象
            client = new Client(config);
            //4.创建请求对象，并设置信息
            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setPhoneNumbers(phone)
                    .setSignName("来自邢朋辉的短信")
                    .setTemplateCode("SMS_114390520")
                    .setTemplateParam("{\"code\":\""+code+"\"}");
            //5.发送短信验证码
            SendSmsResponse response=client.sendSms(sendSmsRequest);
            System.err.println(response.getBody().message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
