package com.yysys.yinyuan.service.intf;

import com.yysys.yinyuan.bo.SmsCodeBo;
import com.yysys.yinyuan.vo.R;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:21
 */
public interface SmsService {
    //发送验证码
    R sendCode(String phone);
    //校验验证码
    R checkCode(SmsCodeBo bo);
    //查询发送记录
    R all();
}
