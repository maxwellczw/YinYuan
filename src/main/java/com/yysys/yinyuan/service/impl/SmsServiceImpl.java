package com.yysys.yinyuan.service.impl;

import com.yysys.yinyuan.bo.SmsCodeBo;
import com.yysys.yinyuan.config.RedisKeyConfig;
import com.yysys.yinyuan.config.SystemConfig;
import com.yysys.yinyuan.dao.SmsLogDao;
import com.yysys.yinyuan.entity.SmsLog;
import com.yysys.yinyuan.service.intf.SmsService;
import com.yysys.yinyuan.third.AliSmsUtil;
import com.yysys.yinyuan.third.JedisUtil;
import com.yysys.yinyuan.util.NumUtil;
import com.yysys.yinyuan.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:23
 */
@Service
public class SmsServiceImpl implements SmsService {
    @Autowired
    private SmsLogDao dao;
    //如何实现唯一登陆？
    @Override
    public R sendCode(String phone) {
        //1.生成验证码
        String key=RedisKeyConfig.CODE_R+phone;
        int code;
        //校验key
        if(JedisUtil.checkKey(key)){
            //验证时间
            if(JedisUtil.ttl(key)>RedisKeyConfig.CODE_R_TIME/2){
                code=Integer.parseInt(JedisUtil.getStr(key));
            }else {
                code= NumUtil.createNum(SystemConfig.CODE_LEN);
            }
        }else {
            code= NumUtil.createNum(SystemConfig.CODE_LEN);
        }
        //2.发送短信
        if(AliSmsUtil.sendCode(phone,code)){
            //3.记录验证码 --Redis  有效期
            JedisUtil.addStr(key,code+"",RedisKeyConfig.CODE_R_TIME);
            //4.更新数据库
            dao.insert(new SmsLog(SystemConfig.SMS_TYPE_1,"注册账号",phone,new Date()));
            //5.返回
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R checkCode(SmsCodeBo bo) {
        String key=RedisKeyConfig.CODE_R+bo.getPhone();
        //校验是否存在
        if(JedisUtil.checkKey(key)){
            //校验验证码是否正确
            if(bo.getCode()==Integer.parseInt(key)){
                //删除验证码 一次性有效
                JedisUtil.delKey(key);
                return R.ok();
            }
        }
        return R.fail();
    }

    @Override
    public R all() {
        return R.ok(dao.selectList(null));
    }
}
