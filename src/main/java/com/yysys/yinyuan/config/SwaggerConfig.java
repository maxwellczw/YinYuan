package com.yysys.yinyuan.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @program: BootStudy
 * @description:
 * @author: czw
 * @create: 2021-06-26 11:28
 */
@Configuration //说明这是一个配置 类似：<beans>
@EnableSwagger2 //启用Swagger
public class SwaggerConfig {

    /**
     * 构建文档的基本信息
     */
    public ApiInfo createApi(){
        return new ApiInfoBuilder().title("浮生项目的接口文档").
                description("浮生项目的接口在线文档,可以浏览整个项目的所有接口，并且可以为接口进行测试")
                .contact(new Contact("老邢","http://www.qfedu.com","xingfei_work@163.com"))
                .version("1.0.0").build();
    }
    /**
     * 就是ioc创建实例 修饰方法 方法必须返回对象
     */
    @Bean //将方法的返回值 存储到IOC容器中 类似<bean>
    public Docket createDocket(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(createApi())
                .select().apis(RequestHandlerSelectors.
                        basePackage("com.feri.idler.controller")).build();
    }

}
