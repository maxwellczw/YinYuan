package com.yysys.yinyuan.config;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:30
 */
public class SystemConfig {
    //验证码的位数
    public static final int CODE_LEN=6;

    //标记短信发送的类型
    public static final int SMS_TYPE_1=1;//注册验证码
    public static final int SMS_TYPE_2=2;//登陆
    public static final int SMS_TYPE_3=3;//找回

    //统一的结果返回码
    public static final int R_OK=10000;
    public static final int R_FAIL=10001;

    //用户的标记位
    public static final int USER_OK=1;
    public static final int USER_FAIL=2;

    //用户操作日志类型
    public static final int USER_LOG_1=1;//注册
    public static final int USER_LOG_2=2;//登陆

}
