package com.yysys.yinyuan.config;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:32
 */
public class RedisKeyConfig {
    //短信验证码-注册 String 类型 key:idler:code:register:手机号 值：验证码
    public static final String CODE_R="idler:code:register:";//追加手机号
    public static final Integer CODE_R_TIME=600;//10分钟


    //
    public static final String USER_TOKEN_Phone="idler:user:phone:";//追加手机号,值，存储对应的令牌
    public static final String USER_TOKEN="idler:user:token:";//追加令牌,对应用户信息
    public static final Integer USER_TOKEN_TIME=1800;//

}
