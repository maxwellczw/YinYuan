package com.yysys.yinyuan.vo;

import com.yysys.yinyuan.config.SystemConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: IdlerApi
 * @description:
 * @author: czw
 * @create: 2021-07-02 16:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class R {
    private int code;
    private String msg;
    private Object data;

    //成功
    public static R ok(Object obj){
        return new R(SystemConfig.R_OK,"OK",obj);
    }
    public static R ok(){
        return new R(SystemConfig.R_OK,"OK",null);
    }
    //失败
    public static R fail(String msg){
        return new R(SystemConfig.R_FAIL,msg,null);
    }
    public static R fail(){
        return new R(SystemConfig.R_FAIL,"FAIL",null);
    }
}
